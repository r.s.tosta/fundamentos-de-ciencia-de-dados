from tinydb import TinyDB,Query
import os

def inserir_db (titulo,link,data,data_atualizada,tags,paragrafos):
    db=TinyDB(f"/home/lantri_rebecatosta/codigo/fundamentos-de-ciencia-de-dados/python/mercosur.json", indent=4, ensure_ascii=False)
    buscar=Query()
    verificar_db=db.contains((buscar.titulo==titulo)& (buscar.data==data))
    if not verificar_db:
        db.insert({
            "titulo":titulo,
            "link":link,
            "data":data,
            "data_atualizada":data_atualizada,
            "tags":tags,
            "paragrafos":paragrafos
             })
    else:
        print("Já está na base")



def atualizar_db ():
    db=TinyDB("/home/lantri_rebecatosta/codigo/fundamentos-de-ciencia-de-dados/python/mercosur.json", indent=4, ensure_ascii=False)
    buscar=Query()
    for info in iter(db):
        #print(info)
        titulo=info["titulo"]
        data=info["data"]
        data_atualizada=info["data_atualizada"]
        paragrafos=info["paragrafos"]
        mes={
            "janeiro":"01",
            "fevereiro":"02",
            "março":"03",
            "abril":"04",
            "maio":"05",
            "junho":"06",
            "julho":"07",
            "agosto":"08",
            "setembro":"09",
            "outubro":"10",
            "novembro":"11",
            "dezembro":"12"
        }
        data_lista=data.split()
        print(data_lista)
        try:
            data_normalizada=f"{data_lista[0]}/{mes[data_lista[2]]}/{data_lista[4]}"
        except:
            data_normalizada=data
        data_atualizada_normalizada=f"{data_atualizada[8:10]}/{data_atualizada[5:7]}/{data_atualizada[0:4]}"
        horario_atualizado=data_atualizada[11:19]
        fuso_horario=data_atualizada[19:]
        if "T" in data_atualizada: 
            db.upsert({
                "data":data_normalizada,
                "data_atualizada":data_atualizada_normalizada,
                "horario_atualizado":horario_atualizado,
                "fuso_horario":fuso_horario
            },buscar.titulo==titulo)
        print(data_normalizada)
        print(data_atualizada_normalizada)
        print(horario_atualizado)
        print(fuso_horario)
        print(paragrafos)

def tratamento_banco(): 
    #retirar /n
    #retirar espaços vazios
    #problemas nos campos data, horario e conteudo
    db=TinyDB("/home/lantri_rebecatosta/codigo/fundamentos-de-ciencia-de-dados/python/db_noticia.json", indent=4, ensure_ascii=False)
    buscar=Query()
    for info in iter(db):
        titulo=info["titulo"]
        data=info["data"].replace("\n","").strip()
        horario=info["horario"].replace("\n","").strip()
        lista_conteudo=info["conteudo"]
        l_conteudo=[paragrafo.replace("\n","").strip() for paragrafo in lista_conteudo]
        conteudo=[]
        for paragrafo in l_conteudo: 
            lista_paragrafo=paragrafo.split()
            paragrafo_final=[]
            for p in lista_paragrafo:
                if " " in p:
                    pass
                else:
                    paragrafo_final.append(p)
            paragrafo_final2=" ".join(paragrafo_final)
            conteudo.append(paragrafo_final2)

        db.upsert({
                "data":data,
                "horario":horario,
                "conteudo":conteudo
            },buscar.titulo==titulo)

def main ():
   #inserir_db ()
   #atualizar_db()
   tratamento_banco()

if __name__ == "__main__":
    main ()
