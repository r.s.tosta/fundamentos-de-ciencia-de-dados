import requests
from bs4 import BeautifulSoup
from banco_json import inserir_db

def acessar_pagina (url):
    pagina = requests.get(url)
    bs=BeautifulSoup (pagina.text, "html.parser")
    #print(bs)
    return bs

def extrair_info ():
    for index, pagina in enumerate(paginas(),start=1):
        print(index,pagina)
        bs= acessar_pagina (pagina)
        noticias=bs.find("div",attrs={"id":"posts-container"}).find("div",attrs={"class":"fusion-posts-container fusion-blog-layout-grid fusion-blog-layout-grid-3 isotope fusion-blog-equal-heights fusion-blog-pagination"}).find_all("article")
        #print(noticias)
        for noticia in noticias: 
            titulo=noticia.find("h2").text
            print(titulo)
            link=noticia.a["href"]
            print(link)
            data=noticia.find("p",attrs={"class":"fusion-single-line-meta"}).find_all("span")[3].text
            data_atualizada=noticia.find("p",attrs={"class":"fusion-single-line-meta"}).find_all("span")[2].text
            print(data)
            print(data_atualizada)
            lista_tags=noticia.find("p",attrs={"class":"fusion-single-line-meta"}).find_all("a")
            tags=[] 
            for tag in lista_tags:
                tags.append(tag.text)
            print(tags)
            page_noticia= acessar_pagina (link)
            tag_paragrafos= page_noticia.find("div",attrs={"class":"post-content"}).find_all("p")
            #print(tag_paragrafos)
            paragrafos=[]
            for paragrafo in tag_paragrafos:
                paragrafos.append(paragrafo.text)
            print(paragrafos)
            print("###")
            inserir_db (titulo,link,data,data_atualizada,tags,paragrafos)

def paginas ():
    #https://www.mercosur.int/pt-br/midia/arquivo-de-noticias/page/58/
    paginas=[]
    contador=58
    while contador>=1:
        url="https://www.mercosur.int/pt-br/midia/arquivo-de-noticias/page/"
        url=url+str(contador)
        paginas.append(url)
        contador=contador-1
    print(paginas)
    return paginas

def main ():
    extrair_info()
    


if __name__ == "__main__":
    main ()
