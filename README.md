# Fundamentos de Ciências de Dados

## Comandos para versionamento (git)

Digitar os comandos abaixo na raiz do projeto para versionar o código e subí-lo no gitlab

```
git add .
git commit -m "insira mensagem aqui"
git pull origin main
git push origin main
```

## Criação do Ambiente Virtual (conda)

```
conda activate env_rebeca
conda env update

```

## Comandos básicos do terminal Linux

```
ls
cd
mkdir
history
cat
```

